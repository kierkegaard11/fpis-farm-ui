import { ReceiptItem } from "./receipt-item.model";

export class ReceiptSaveJson {
  constructor(
    public dueDate: string,
    public note: string,
    public paymentMethodIds: number[],
    public items: ReceiptItem[],
    public deliveryNoteId: number,
    public workerId: number,
    public customerId: number
  ) {}
}
