import { DeliveryNote } from "./delivery-note.model";
import { PaymentMethod } from "./payment-method.modal";
import { ReceiptItem } from "./receipt-item.model";

export class Receipt {
  constructor(
    public id: number,
    public dueDate: Date,
    public note: string,
    public paymentMethods: PaymentMethod[],
    public items: ReceiptItem[],
    public deliveryNote: DeliveryNote,
    public workerId: number,
    public customerId: number
  ) {}
}
