export class ReceiptItem {
  constructor(
    public id: number,
    public productId: number,
    public quantity: number,
    public rebate: number,
    public priceWithDiscount: number,
    public productName: string
  ) {}
}

