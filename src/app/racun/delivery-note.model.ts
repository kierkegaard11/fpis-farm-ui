export class DeliveryNote {
  constructor(
      public id: number,
      public customerId: number,
      public dispatchDate: Date,
      public workerDelivered: number,
      public workerIssued: number
  ) {}
}
