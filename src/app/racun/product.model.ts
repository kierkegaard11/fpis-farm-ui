import { Unit } from "./unit.model";

export class Product {
  constructor(
      public id: number,
      public name: string,
      public unit: Unit,
      public dateCreated: Date,
      public price: number,
      public productTypeId: number,
      public quantityOnStock: number
  ) {}
}

