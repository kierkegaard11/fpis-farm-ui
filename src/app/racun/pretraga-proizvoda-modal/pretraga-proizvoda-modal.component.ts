import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { RequestsService } from 'src/app/requests.service';
import { ProductTableEntry } from '../product-table-entry.model';
import { Product } from '../product.model';

@Component({
  selector: 'app-pretraga-proizvoda-modal',
  templateUrl: './pretraga-proizvoda-modal.component.html',
  styleUrls: ['./pretraga-proizvoda-modal.component.scss']
})
export class PretragaProizvodaModalComponent implements OnInit {

  proizvodId: string = "";
  proizvodIdErrorMessage: string = "";

  nazivProizvoda: string = "";
  nazivProizvodaErrorMessage: string;

  proizvodiTableData: ProductTableEntry[] = [];
  proizvodiDisplayColumns: string[] = ['proizvodId', 'naziv', 'jedinicaMere', 'datumProizvodnje', 'cena', 'tipProizvoda', 'kolicinaNaStanju'];

  selectedProduct: Product;

  constructor(
    private datepipe: DatePipe,
    private requestsService: RequestsService,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<PretragaProizvodaModalComponent>
  ) { }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  onPretragaProizvoda() {
    this.proizvodiTableData = [];

    this.proizvodIdErrorMessage = "";
    this.nazivProizvodaErrorMessage = "";

    if (this.proizvodId != "") {
      if (!this.validateId()) {
        return;
      }
    }

    this.requestsService.getProducts(this.proizvodId, this.nazivProizvoda).then(
      (proizvodi: Product[]) => {
        console.log("Product service response", proizvodi);

        const newProizvodiTableData: ProductTableEntry[] = [];

        proizvodi.forEach(proizvod => {
          newProizvodiTableData.push(this.convertProductToProductTableEntry(proizvod));
        });

        this.proizvodiTableData = newProizvodiTableData;
      }, errRes => {
        this.openSnackBar("Greska pri pribavljanju proizvoda.", "OK");
      }
    );
  }

  onIzaberiProizvod() {
    if (this.selectedProduct != null || this.selectedProduct != undefined) {
      this.dialogRef.close(this.selectedProduct);
    } else {
      this.openSnackBar('Prvo izaberite proizvod pritiskom na "Pronadji proizvod" dugme i selektovanjem proizvoda iz tabele.', "OK");
    }
  }

  onOdustani() {
    this.dialogRef.close();
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  validateId() {
    if (!Number.isInteger(Number(this.proizvodId))) {
      this.proizvodIdErrorMessage = "Sifra proizvoda mora biti celobrojna vrednost.";
      return false;
    }

    if (Number(this.proizvodId) == 0) {
      this.proizvodIdErrorMessage = "Sifra proizvoda ne moze biti 0.";
      return false;
    }

    if (this.proizvodId.charAt(0) == '0') {
      this.proizvodIdErrorMessage = "Sifra proizvoda ne moze poceti sa 0.";
      return false;
    }

    this.proizvodIdErrorMessage = "";
    return true;
  }

  convertProductTableEntryToProduct(pte: ProductTableEntry) {
    const product = new Product(
      pte.id,
      pte.name,
      pte.unit,
      pte.dateCreated,
      pte.price,
      pte.productTypeId,
      pte.quantityOnStock
    )

    return product;
  }

  convertProductToProductTableEntry(product: Product) {
    const pte = new ProductTableEntry(
      product.id,
      product.name,
      product.unit,
      product.dateCreated,
      product.price,
      product.productTypeId,
      product.quantityOnStock,
      false
    )

    return pte;
  }

  onTableRowSelect(pte: ProductTableEntry) {
    this.proizvodiTableData.forEach(proizvod => {
      proizvod.isSelected = false;
    });

    pte.isSelected = true;

    this.selectedProduct = this.convertProductTableEntryToProduct(pte);
  }
}
