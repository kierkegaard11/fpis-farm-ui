import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretragaProizvodaModalComponent } from './pretraga-proizvoda-modal.component';

describe('PretragaProizvodaModalComponent', () => {
  let component: PretragaProizvodaModalComponent;
  let fixture: ComponentFixture<PretragaProizvodaModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretragaProizvodaModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretragaProizvodaModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
