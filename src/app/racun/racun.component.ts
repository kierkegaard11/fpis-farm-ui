import { DatePipe } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Worker } from './worker.model';
import { RequestsService } from '../requests.service';
import { DeliveryNote } from './delivery-note.model';
import { PaymentMethod } from './payment-method.modal';
import { PretragaOtpremniceModalComponent } from './pretraga-otpremnice-modal/pretraga-otpremnice-modal.component';
import { PretragaProizvodaModalComponent } from './pretraga-proizvoda-modal/pretraga-proizvoda-modal.component';
import { ProductTableEntry } from './product-table-entry.model';
import { Product } from './product.model';
import { ReceiptItemTableEntry } from './receipt-item-table-entry.model';
import { ReceiptItem } from './receipt-item.model';
import { ReceiptSaveJson } from './receipt-save-json.model';
import { Receipt } from './receipt.model';

@Component({
  selector: 'app-racun',
  templateUrl: './racun.component.html',
  styleUrls: ['./racun.component.scss']
})
export class RacunComponent implements OnInit {

  rokZaPlacanje: string = "";
  rokZaPlacanjeErrorMessage: string;

  napomenaRacunaKupca: string = "";
  napomenaRacunaKupcaErrorMessage: string;

  naciniPlacanja: PaymentMethod[] = [];

  nacinPlacanjaTableData: PaymentMethod[] = [];
  nacinPlacanjaDisplayColumns: string[] = ['nacinPlacanja'];

  otpremnicaTableData: DeliveryNote[] = [];
  otpremnicaDisplayColumns: string[] = ['id', 'datumOtpremanja', 'kupacId', 'radnikDopremioId', 'radnikIznadId'];

  proizvodiTableData: ProductTableEntry[] = [];
  proizvodiDisplayColumns: string[] = ['proizvodId', 'naziv', 'jedinicaMere', 'datumProizvodnje', 'cena', 'tipProizvoda', 'kolicinaNaStanju'];

  narucenaKolicina: string = "";
  narucenaKolicinaErrorMessage: string;

  popust: string = "";
  popustErrorMessage: string;

  proizvodStavkeTableData: ReceiptItemTableEntry[] = [];
  proizvodStavkeDisplayColumns: string[] = ['proizvodId', 'kolicina', 'popust', 'cenaStavke', 'nazivProizvoda'];

  radnici: Worker[] = [];

  proizvodDaSeDoda: Product;

  selektovanaStavkaTableEntry: ReceiptItemTableEntry;

  selektovaniRadnk: Worker = undefined;

  @Input() componentIsChildComponent: boolean = false;
  @Input() selektovaniParentRacun: Receipt;

  constructor(
    private datepipe: DatePipe,
    private dialog: MatDialog,
    private requestsService: RequestsService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    if (this.componentIsChildComponent) {
      this.fillOutReceipt();
    }

    this.loadPaymentMethods();
    this.loadWorkers();
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  onNacinPlacanjaSelect(nacinPlacanja: PaymentMethod) {
    const tempTableData = [...this.nacinPlacanjaTableData];

    let add = true;

    tempTableData.forEach(np => {
      if (np.id == nacinPlacanja.id) {
        this.openSnackBar("Nacin placanja je vec dodat", "OK");
        add = false;
      }
    });

    if (add) {
      tempTableData.push(nacinPlacanja);
    }

    this.nacinPlacanjaTableData = tempTableData;

    setTimeout(() => {
      var objDiv = document.getElementById("nacin-placanja-table");
      objDiv.scrollTop = objDiv.scrollHeight;
    }, 50);
  }

  onPretragaOtpremnice() {
    const dialogRef = this.dialog.open(PretragaOtpremniceModalComponent, {
      height: '700px',
      width: '650px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        this.otpremnicaTableData = [];
        if (result != null || result != undefined) {
          this.otpremnicaTableData.push(result);
        }
      }
    });
  }

  onPretragaProizvoda() {
    const dialogRef = this.dialog.open(PretragaProizvodaModalComponent, {
      height: '700px',
      width: '650px'
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result != undefined) {
        const tempTableData = [...this.proizvodiTableData];

        if (tempTableData.find(product => product.id == result.id) != undefined) {
          this.openSnackBar("Proizvod je vec dodat", "OK");
          return;
        }

        tempTableData.push(result);

        this.proizvodiTableData = tempTableData;
      }
    });
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  onDodajStavku() {
    if (this.proizvodDaSeDoda != null || this.proizvodDaSeDoda != undefined) {
      if (this.narucenaKolicinaCheck() && this.popustCheck()) {
        const tempTableData = [...this.proizvodStavkeTableData];

        const pite = new ReceiptItemTableEntry(
          undefined,
          this.proizvodDaSeDoda.id,
          +this.narucenaKolicina,
          +this.popust,
          (Number(this.narucenaKolicina) * Number(this.proizvodDaSeDoda.price)) - (Number(this.narucenaKolicina) * Number(this.proizvodDaSeDoda.price) * Number(this.popust)),
          this.proizvodDaSeDoda.name,
          false
        )

        tempTableData.push(pite);

        this.proizvodStavkeTableData = tempTableData;

        this.proizvodiTableData.forEach(proizvod => {
          proizvod.isSelected = false;
        });

        this.narucenaKolicina = "";
        this.popust = "";

        this.proizvodDaSeDoda = undefined;

        this.proizvodStavkeTableData.forEach(stavka => {
          stavka.isSelected = false;
        });

        this.selektovanaStavkaTableEntry = undefined;
      }
    } else {
      this.openSnackBar('Prvo odaberite proizvod iz tabele', "OK");
    }
  }

  onIzmeniStavku() {
    console.log("Stavka", this.selektovanaStavkaTableEntry);
    if (this.selektovanaStavkaTableEntry == null || this.selektovanaStavkaTableEntry == undefined) {
      this.openSnackBar("Prvo selektujte stavku racuna", "OK");
      return;
    }

    if (!(this.narucenaKolicinaCheck() && this.popustCheck())) {
      return;
    }

    const tempTableData = [...this.proizvodStavkeTableData];

    tempTableData.forEach(stavka => {
      if (stavka == this.selektovanaStavkaTableEntry) {
        console.log("this.narucenaKolicina", this.narucenaKolicina);
        console.log("this.proizvodDaSeDoda.price", this.proizvodDaSeDoda.price);
        console.log("this.popust", this.popust);

        stavka.productId = this.proizvodDaSeDoda.id;
        stavka.quantity = Number(this.narucenaKolicina);
        stavka.rebate = Number(this.popust);
        stavka.priceWithDiscount = (Number(this.narucenaKolicina) * Number(this.proizvodDaSeDoda.price)) - (Number(this.narucenaKolicina) * Number(this.proizvodDaSeDoda.price) * Number(this.popust));
        stavka.productName = this.proizvodDaSeDoda.name;
      }
    });

    this.proizvodStavkeTableData = tempTableData;
  }

  onObrisiStavku() {
    if (this.selektovanaStavkaTableEntry == null || this.selektovanaStavkaTableEntry == undefined) {
      this.openSnackBar("Prvo selektujte stavku racuna", "OK");
      return;
    }

    const tempTableData = [];

    this.proizvodStavkeTableData.forEach(stavka => {
      if (stavka != this.selektovanaStavkaTableEntry) {
        tempTableData.push(stavka);
      }
    });

    this.proizvodStavkeTableData = tempTableData;
    this.selektovanaStavkaTableEntry = undefined;
  }

  onPotvrdi() {
    if (!this.validateDate()) {
      this.openSnackBar("Rok za placanje nije ispravan.", "OK");
      return;
    }

    if (this.nacinPlacanjaTableData.length == 0) {
      this.openSnackBar("Morate izabrati bar jedan nacin placanja.", "OK");
      return;
    }

    if (this.otpremnicaTableData.length == 0) {
      this.openSnackBar("Otpremnica nije izabrana.", "OK");
      return;
    }

    if (this.selektovaniRadnk == null || this.selektovaniRadnk == undefined) {
      this.openSnackBar("Radnik nije izabran.", "OK");
      return false;
    }

    if (this.proizvodStavkeTableData.length == 0) {
      this.openSnackBar("Racun mora imati bar jednu stavku.", "OK");
      return;
    }

    let naciniPlacanjaIDs: number[] = [];

    this.nacinPlacanjaTableData.forEach(paymentMethod => {
      naciniPlacanjaIDs.push(paymentMethod.id);
    });


    let racun = new ReceiptSaveJson(
      this.rokZaPlacanje,
      this.napomenaRacunaKupca,
      naciniPlacanjaIDs,
      this.getStavkeRacuna(),
      this.otpremnicaTableData[0].id,
      this.selektovaniRadnk.workerId,
      this.otpremnicaTableData[0].customerId,
    );

    console.log("Racun", racun);
    console.log(JSON.stringify(racun));

    this.requestsService.saveReceipt(racun).then(
      (response: any) => {
        this.openSnackBar("Racun uspesno sacuvan!.", "OK");

        this.rokZaPlacanje = "";
        this.rokZaPlacanjeErrorMessage = ""
        this.napomenaRacunaKupca = "";
        this.napomenaRacunaKupcaErrorMessage = "";
        this.naciniPlacanja = [];
        this.nacinPlacanjaTableData = [];
        this.otpremnicaTableData = [];
        this.proizvodiTableData = [];
        this.narucenaKolicina = "";
        this.narucenaKolicinaErrorMessage = "";
        this.popust = "";
        this.popustErrorMessage = "";
        this.proizvodStavkeTableData = [];
        this.proizvodDaSeDoda = undefined;
        this.selektovanaStavkaTableEntry = undefined;
        this.selektovaniRadnk = undefined;
      }, errRes => {
        this.openSnackBar("Greska pri cuvanju racuna.", "OK");
      }
    );
  }

  loadPaymentMethods() {
    this.requestsService.getPaymentMethods().then(
      (paymentMethods: any) => {
        this.naciniPlacanja = paymentMethods;
      }, errRes => {
        this.openSnackBar("Greska pri pribavljanju nacina placanja.", "OK");
      }
    );
  }

  loadWorkers() {
    this.requestsService.getWorkers().then(
      (workers: Worker[]) => {
        this.radnici = workers;

        if (this.componentIsChildComponent) {
          this.radnici.forEach(worker => {
            if (worker.workerId == this.selektovaniParentRacun.workerId) {
              this.selektovaniRadnk = worker;
            }
          })
        }
      }, errRes => {
        this.openSnackBar("Greska pri pribavljanju radnika.", "OK");
      }
    );
  }

  convertProductTableEntryToProduct(pte: ProductTableEntry) {
    const product = new Product(
      pte.id,
      pte.name,
      pte.unit,
      pte.dateCreated,
      pte.price,
      pte.productTypeId,
      pte.quantityOnStock
    )

    return product;
  }

  convertProductToProductTableEntry(product: Product) {
    const pte = new ProductTableEntry(
      product.id,
      product.name,
      product.unit,
      product.dateCreated,
      product.price,
      product.productTypeId,
      product.quantityOnStock,
      false
    )

    return pte;
  }

  onProductTableRowSelect(pte: ProductTableEntry) {
    this.proizvodiTableData.forEach(proizvod => {
      proizvod.isSelected = false;
    });

    pte.isSelected = true;

    this.proizvodDaSeDoda = this.convertProductTableEntryToProduct(pte);
  }

  narucenaKolicinaCheck() {
    if (this.narucenaKolicina == "") {
      this.narucenaKolicinaErrorMessage = "Narucena kolicina je obavezno polje.";
      return false;
    }

    if (Number.isNaN(Number(this.narucenaKolicina))) {
      this.narucenaKolicinaErrorMessage = "Narucena kolicina mora da bude numericka vrednost.";
      return false;
    }

    if (Number(this.narucenaKolicina) === 0) {
      this.narucenaKolicinaErrorMessage = "Narucena kolicina ne moze biti 0.";
      return false;
    }

    this.narucenaKolicinaErrorMessage = "";
    return true;
  }

  popustCheck() {
    if (this.popust == "") {
      this.popustErrorMessage = "Popust je obavezno polje.";
      return false;
    }

    if (Number.isNaN(Number(this.popust))) {
      this.popustErrorMessage = "Popust mora da bude numericka vrednost.";
      return false;
    }

    if (Number(this.popust) < 0 || Number(this.popust) > 1) {
      this.popustErrorMessage = "Vrednost popusta mora da bude od 0 do 1.";
      return false;
    }

    this.popustErrorMessage = "";
    return true;
  }

  onItemsTableRowSelect(rite: ReceiptItemTableEntry) {
    this.proizvodStavkeTableData.forEach(stavka => {
      stavka.isSelected = false;
    });

    rite.isSelected = true;

    this.selektovanaStavkaTableEntry = rite;

    this.proizvodiTableData.forEach(proizvodTableEntry => {
      if (proizvodTableEntry.id == this.selektovanaStavkaTableEntry.productId) {
        this.proizvodiTableData.forEach(proizvod => {
          proizvod.isSelected = false;
        });

        proizvodTableEntry.isSelected = true;

        this.proizvodDaSeDoda = this.convertProductTableEntryToProduct(proizvodTableEntry);
        this.narucenaKolicina = `${this.selektovanaStavkaTableEntry.quantity}`;
        this.popust = `${this.selektovanaStavkaTableEntry.rebate}`;
      }
    });
  }

  convertReceiptItemTableEntryToReceiptItem(rite: ReceiptItemTableEntry) {
    const receiptItem = new ReceiptItem(
      rite.id,
      rite.productId,
      rite.quantity,
      rite.rebate,
      rite.priceWithDiscount,
      rite.productName
    )

    return receiptItem;
  }

  convertReceiptItemToReceiptItemTableEntry(receiptItem: ReceiptItem) {
    const rite = new ReceiptItemTableEntry(
      receiptItem.id,
      receiptItem.productId,
      receiptItem.quantity,
      receiptItem.rebate,
      receiptItem.priceWithDiscount,
      receiptItem.productName,
      false
    )

    return rite;
  }

  onRadnikSelect(radnik: Worker) {
    this.selektovaniRadnk = radnik;
  }

  validateDate() {
    if (this.rokZaPlacanje.length == 0) {
      this.rokZaPlacanjeErrorMessage = "Rok za placanje je obavezno polje.";
      return false;
    }

    if (!this.isValidDateFormat(this.rokZaPlacanje)) {
      this.rokZaPlacanjeErrorMessage = "Rok za placanje nije u formatu yyyy-mm-dd.";
      return false;
    }

    if (Number(this.rokZaPlacanje.split("-")[1]) == 0 || Number(this.rokZaPlacanje.split("-")[1]) > 12) {
      this.rokZaPlacanjeErrorMessage = "Rok za placanje nije ispravan.";
      return false;
    }

    if (Number(this.rokZaPlacanje.split("-")[2]) == 0 || Number(this.rokZaPlacanje.split("-")[2]) > 31) {
      this.rokZaPlacanjeErrorMessage = "Rok za placanje nije ispravan.";
      return false;
    }

    this.rokZaPlacanjeErrorMessage = "";
    return true;
  }

  isValidDateFormat(str: string) {
    return /^\d{4}\-\d{1,2}\-\d{1,2}$/.test(str);
  }

  getStavkeRacuna(): ReceiptItem[] {
    const stavke: ReceiptItem[] = [];

    this.proizvodStavkeTableData.forEach((receiptItemTableEntry: ReceiptItemTableEntry) => {
      stavke.push(this.convertReceiptItemTableEntryToReceiptItem(receiptItemTableEntry));
    });

    return stavke;
  }

  fillOutReceipt() {
    // popunjavanje text fieldova
    this.rokZaPlacanje = this.dateFormat(this.selektovaniParentRacun.dueDate);
    this.napomenaRacunaKupca = this.selektovaniParentRacun.note;

    // popunjavanja nacina placanja
    const tempNacinPlacanjaTableData = [...this.nacinPlacanjaTableData];

    this.selektovaniParentRacun.paymentMethods.forEach(paymentMethod => {

      tempNacinPlacanjaTableData.push(paymentMethod);
    });

    this.nacinPlacanjaTableData = tempNacinPlacanjaTableData;

    // popunjavanje otpremnice
    const tempOtpremnicaTableData = [...this.otpremnicaTableData];

    tempOtpremnicaTableData.push(this.selektovaniParentRacun.deliveryNote);

    this.otpremnicaTableData =   tempOtpremnicaTableData;

    // popunjavanja stavki racuna

    const tempProizvodStavkeTableData = [...this.proizvodStavkeTableData];

    this.selektovaniParentRacun.items.forEach(item => {
      tempProizvodStavkeTableData.push(this.convertReceiptItemToReceiptItemTableEntry(item));
    });

    this.proizvodStavkeTableData = tempProizvodStavkeTableData;

    // popunjavanje tabele proizvoda proizvodima koji se pojavljuju u stavci

    this.requestsService.getProducts("", "").then(
      products => {
        const tempProizvodiTableData = [...this.proizvodiTableData];

        products.forEach(product => {
          this.proizvodStavkeTableData.forEach(item => {
            if (product.id == item.productId) {
              tempProizvodiTableData.push(this.convertProductToProductTableEntry(product));
            }
          });
        });

        this.proizvodiTableData = tempProizvodiTableData;

        // note: naziv radnika se popuni kada se ucitaju radnici
      }, errRes => {
        console.log("Error getting products", errRes);
        this.openSnackBar("Greska u popunjavanju racuna!", "OK");
      }
    )
  }

  exportJsonData() {
    if (!this.validateDate()) {
      this.openSnackBar("Rok za placanje nije ispravan.", "OK");
      return undefined;
    }

    if (this.nacinPlacanjaTableData.length == 0) {
      this.openSnackBar("Morate izabrati bar jedan nacin placanja.", "OK");
      return undefined;
    }

    if (this.otpremnicaTableData.length == 0) {
      this.openSnackBar("Otpremnica nije izabrana.", "OK");
      return undefined;
    }

    if (this.selektovaniRadnk == null || this.selektovaniRadnk == undefined) {
      this.openSnackBar("Radnik nije izabran.", "OK");
      return undefined;
    }

    if (this.proizvodStavkeTableData.length == 0) {
      this.openSnackBar("Racun mora imati bar jednu stavku.", "OK");
      return undefined;
    }

    let naciniPlacanjaIDs: number[] = [];

    console.log("nacin placanja tabled", this.nacinPlacanjaTableData)
    this.nacinPlacanjaTableData.forEach(paymentMethod => {
      naciniPlacanjaIDs.push(paymentMethod.id);
    });


    let racun = new ReceiptSaveJson(
      this.rokZaPlacanje,
      this.napomenaRacunaKupca,
      naciniPlacanjaIDs,
      this.getStavkeRacuna(),
      this.otpremnicaTableData[0].id,
      this.selektovaniRadnk.workerId,
      this.otpremnicaTableData[0].customerId,
    );

    return racun;
  }
}
