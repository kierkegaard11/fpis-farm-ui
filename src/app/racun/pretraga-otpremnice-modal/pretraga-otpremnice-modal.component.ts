import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatSnackBar } from '@angular/material';
import { RequestsService } from 'src/app/requests.service';
import { DeliveryNoteTableEntry } from '../delivery-note-table-entry.model';
import { DeliveryNote } from '../delivery-note.model';

@Component({
  selector: 'app-pretraga-otpremnice-modal',
  templateUrl: './pretraga-otpremnice-modal.component.html',
  styleUrls: ['./pretraga-otpremnice-modal.component.scss']
})
export class PretragaOtpremniceModalComponent implements OnInit {

  otpremnicaId: string = "";
  otpremnicaIdErrorMessage: string;

  datumOtpremnice: string = "";
  datumOtpremniceErrorMessage: string;

  otpremnicaTableData: DeliveryNoteTableEntry[] = [];
  otpremnicaDisplayColumns: string[] = ['id', 'datumOtpremanja', 'kupacId', 'radnikDopremioId', 'radnikIznadId'];

  selectedDeliveryNote: DeliveryNote;

  constructor(
    private datepipe: DatePipe,
    private requestsService: RequestsService,
    private snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<PretragaOtpremniceModalComponent>
  ) { }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  onPretragaOtpremnice() {
    this.otpremnicaTableData = [];

    this.otpremnicaIdErrorMessage = "";
    this.datumOtpremniceErrorMessage = "";

    if (this.otpremnicaId != "") {
      if (!this.validateId()) {
        return;
      }
    }

    if (this.datumOtpremnice != "") {
      if (!this.validateDate()) {
        return;
      }
    }

    this.requestsService.getDeliveryNotes(this.otpremnicaId, this.datumOtpremnice).then(
      (deliveryNotes: DeliveryNote[]) => {
        console.log("Delivery note service response", deliveryNotes);

        const newOtpremnicaTableData: DeliveryNoteTableEntry[] = [];

        deliveryNotes.forEach(deliveryNote => {
          newOtpremnicaTableData.push(this.convertDeliveryNoteToDeliveryNoteTableEntry(deliveryNote));
        });

        this.otpremnicaTableData = newOtpremnicaTableData;
      }, errRes => {
        this.openSnackBar("Greska pri pribavljanju nacina placanja.", "OK");
      }
    );
  }

  onIzaberiOtpremnicu() {
    if (this.selectedDeliveryNote != null || this.selectedDeliveryNote != undefined) {
      this.dialogRef.close(this.selectedDeliveryNote);
    } else {
      this.openSnackBar('Prvo izaberite otpremnicu pritiskom na "Pronadji otpremnicu" dugme i selektovanjem otpremnice iz tabele.', "OK");
    }
  }

  onOdustani() {
    this.dialogRef.close();
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  validateId() {
    if (!Number.isInteger(Number(this.otpremnicaId))) {
      this.otpremnicaIdErrorMessage = "Sifra otpremnice mora biti celobrojna vrednost.";
      return false;
    }

    if (Number(this.otpremnicaId) == 0) {
      this.otpremnicaIdErrorMessage = "Sifra otpremnice ne moze biti 0.";
      return false;
    }

    if (this.otpremnicaId.charAt(0) == '0') {
      this.otpremnicaIdErrorMessage = "Sifra otpremnice ne moze poceti sa 0.";
      return false;
    }

    this.otpremnicaIdErrorMessage = "";
    return true;
  }

  validateDate() {
    if (this.datumOtpremnice.length == 0) {
      this.datumOtpremniceErrorMessage = "Datum otpremnice je obavezno polje.";
      return false;
    }

    if (!this.isValidDateFormat(this.datumOtpremnice)) {
      this.datumOtpremniceErrorMessage = "Datum otpremnice nije u formatu yyyy-mm-dd.";
      return false;
    }

    if (Number(this.datumOtpremnice.split("-")[1]) == 0 || Number(this.datumOtpremnice.split("-")[1]) > 12) {
      this.datumOtpremniceErrorMessage = "Datum otpremnice nije ispravna.";
      return false;
    }

    if (Number(this.datumOtpremnice.split("-")[2]) == 0 || Number(this.datumOtpremnice.split("-")[2]) > 31) {
      this.datumOtpremniceErrorMessage = "Datum otpremnice nije ispravna.";
      return false;
    }

    this.datumOtpremniceErrorMessage = "";
    return true;
  }

  isValidDateFormat(str: string) {
    return /^\d{4}\-\d{1,2}\-\d{1,2}$/.test(str);
  }

  convertDeliveryNoteToDeliveryNoteTableEntry(deliveryNote: DeliveryNote) {
    const deliveryNoteTableEntry = new DeliveryNoteTableEntry(
      deliveryNote.id,
      deliveryNote.customerId,
      deliveryNote.dispatchDate,
      deliveryNote.workerDelivered,
      deliveryNote.workerIssued,
      false
    )

    return deliveryNoteTableEntry;
  }

  convertDeliveryNoteTableEntryToDeliveryNote(dnte: DeliveryNoteTableEntry) {
    const deliveryNote = new DeliveryNote(
      dnte.id,
      dnte.customerId,
      dnte.dispatchDate,
      dnte.workerDelivered,
      dnte.workerIssued
    )

    return deliveryNote;
  }

  onTableRowSelect(dnte: DeliveryNoteTableEntry) {
    this.otpremnicaTableData.forEach(deliveryNote => {
      deliveryNote.isSelected = false;
    });

    dnte.isSelected = true;

    this.selectedDeliveryNote = this.convertDeliveryNoteTableEntryToDeliveryNote(dnte);
  }
}
