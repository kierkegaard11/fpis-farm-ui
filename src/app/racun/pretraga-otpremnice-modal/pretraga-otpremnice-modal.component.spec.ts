import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PretragaOtpremniceModalComponent } from './pretraga-otpremnice-modal.component';

describe('PretragaOtpremniceModalComponent', () => {
  let component: PretragaOtpremniceModalComponent;
  let fixture: ComponentFixture<PretragaOtpremniceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PretragaOtpremniceModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PretragaOtpremniceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
