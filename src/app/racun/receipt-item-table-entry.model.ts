export class ReceiptItemTableEntry {
  constructor(
    public id,
    public productId: number,
    public quantity: number,
    public rebate: number,
    public priceWithDiscount: number,
    public productName: string,
    public isSelected: boolean
  ) {}
}

