import { Unit } from "./unit.model";

export class ProductTableEntry {
  constructor(
      public id: number,
      public name: string,
      public unit: Unit,
      public dateCreated: Date,
      public price: number,
      public productTypeId: number,
      public quantityOnStock: number,
      public isSelected: boolean
  ) {}
}

