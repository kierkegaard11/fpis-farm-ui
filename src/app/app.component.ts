import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'someUI';

  constructor() {
    localStorage.setItem('formTwoTableOne', null);
    localStorage.setItem('selectedSertifikat', null);
    localStorage.setItem('formTwoTableTwo', null);
  }
}