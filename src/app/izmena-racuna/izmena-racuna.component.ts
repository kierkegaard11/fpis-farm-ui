import { DatePipe } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { RacunComponent } from '../racun/racun.component';
import { ReceiptTableEntry } from '../racun/receipt-table-entry.model';
import { Receipt } from '../racun/receipt.model';
import { RequestsService } from '../requests.service';

@Component({
  selector: 'app-izmena-racuna',
  templateUrl: './izmena-racuna.component.html',
  styleUrls: ['./izmena-racuna.component.scss']
})
export class IzmenaRacunaComponent implements OnInit {

  @ViewChild(RacunComponent, {static: false}) racunChildComponent: RacunComponent;

  iDRacuna: string = "";
  iDRacunaErrorMessage: string;

  rokPlacanja: string = "";
  rokPlacanjaErrorMessage: string;

  racuniTableData: ReceiptTableEntry[] = [];
  racuniDisplayColumns: string[] = ['racunId', 'rokPlacanja', 'napomena', 'otpremnicaId', 'kupacId', 'radnikId'];

  selektovaniRacun: Receipt;

  racun: Receipt;

  racunIdUneditable: string = "";

  racunSelektovan: boolean = false;

  racunLoading = false;

  constructor(
    private datepipe: DatePipe,
    private snackBar: MatSnackBar,
    private requestsService: RequestsService
  ) { }

  ngOnInit() {

  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  onReceiptTableRowSelect(rte: ReceiptTableEntry) {
    this.racuniTableData.forEach(racun => {
      racun.isSelected = false;
    });

    rte.isSelected = true;

    this.selektovaniRacun = this.convertReceiptTableEntryToReceipt(rte);

    console.log(this.selektovaniRacun);
  }

  convertReceiptTableEntryToReceipt(rte: ReceiptTableEntry) {
    const receipt = new Receipt(
      rte.id,
      rte.dueDate,
      rte.note,
      rte.paymentMethods,
      rte.items,
      rte.deliveryNote,
      rte.workerId,
      rte.customerId
    );

    return receipt;
  }

  convertReceiptToReceiptTableEntry(receipt: Receipt) {
    const rte = new ReceiptTableEntry(
      receipt.id,
      receipt.dueDate,
      receipt.note,
      receipt.paymentMethods,
      receipt.items,
      receipt.deliveryNote,
      receipt.workerId,
      receipt.customerId,
      false
    )

    return rte;
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  onPretragaRacuna() {
    this.racuniTableData = [];
    this.selektovaniRacun = null;

    this.iDRacunaErrorMessage = "";
    this.rokPlacanjaErrorMessage = "";

    if (this.iDRacuna != "") {
      if (!this.validateId()) {
        return;
      }
    }

    if (this.rokPlacanja != "") {
      if (!this.validateDate()) {
        return;
      }
    }

    this.requestsService.getReceipts(this.iDRacuna, this.rokPlacanja).then(
      (racuni: Receipt[]) => {
        console.log(racuni);
        const tempTableData = [...this.racuniTableData];

        racuni.forEach(racun => {
          tempTableData.push(this.convertReceiptToReceiptTableEntry(racun));
        });

        this.racuniTableData = tempTableData;
      }, errRes => {
        console.log("Greska pri pretrazi racuna", errRes);
        this.openSnackBar("Greska pri pretrazi racuna", "OK");
        console.log()
      }
    )
  }

  validateId() {
    if (!Number.isInteger(Number(this.iDRacuna))) {
      this.iDRacunaErrorMessage = "Sifra racuna mora biti celobrojna vrednost.";
      return false;
    }

    if (Number(this.iDRacuna) == 0) {
      this.iDRacunaErrorMessage = "Sifra racuna ne moze biti 0.";
      return false;
    }

    if (this.iDRacuna.charAt(0) == '0') {
      this.iDRacunaErrorMessage = "Sifra racuna ne moze poceti sa 0.";
      return false;
    }

    this.iDRacunaErrorMessage = "";
    return true;
  }

  validateDate() {
    if (!this.isValidDateFormat(this.rokPlacanja)) {
      this.rokPlacanjaErrorMessage = "Rok placanja nije u formatu yyyy-mm-dd.";
      return false;
    }

    if (Number(this.rokPlacanja.split("-")[1]) == 0 || Number(this.rokPlacanja.split("-")[1]) > 12) {
      this.rokPlacanjaErrorMessage = "Rok placanja nije ispravan.";
      return false;
    }

    if (Number(this.rokPlacanja.split("-")[2]) == 0 || Number(this.rokPlacanja.split("-")[2]) > 31) {
      this.rokPlacanjaErrorMessage = "Rok placanja nije ispravan.";
      return false;
    }

    this.rokPlacanjaErrorMessage = "";
    return true;
  }

  isValidDateFormat(str: string) {
    return /^\d{4}\-\d{1,2}\-\d{1,2}$/.test(str);
  }

  onizaberiRacun() {
    this.racunLoading = true;

    if (this.racunSelektovan) {
      this.racunSelektovan = false;
    }

    if (this.selektovaniRacun != null && this.selektovaniRacun != undefined) {
      this.racunIdUneditable = "" + this.selektovaniRacun.id;
      this.racun = Object.assign(this.selektovaniRacun);
      setTimeout(() => {
        this.racunSelektovan = true;
        this.racunLoading = false;
      }, 650);
    } else {
      this.racunIdUneditable = "";
      this.racunLoading = false;
      this.racunSelektovan = false;

      this.openSnackBar("Racun nije selektovan.", "OK");
    }
  }

  onIzmeniRacun() {
    if (this.racunChildComponent.exportJsonData() == undefined || this.racunChildComponent.exportJsonData() == null) {
      return;
    }

    console.log(JSON.stringify(this.racunChildComponent.exportJsonData()));

    if (this.racunSelektovan) {
      this.requestsService.editReceipt(this.racun.id, this.racunChildComponent.exportJsonData()).then(
        response => {
          this.openSnackBar("Racun uspesno izmenjen.", "OK");

          this.racuniTableData.forEach(racun => {
            racun.isSelected = false;
          });

          this.onPretragaRacuna();

          this.selektovaniRacun = undefined;
          this.racun = undefined;
          this.racunIdUneditable = "";
          this.racunSelektovan = false;
        }, errRes => {
          console.log("Error changing receipt", errRes);
        }
      )
    } else {
      this.openSnackBar("Racun nije selektovan", "OK");
    }
}

  onObrisiRacun() {
    if (this.racunSelektovan) {
      this.requestsService.deleteRacun(this.racun.id).then(
        response => {
          this.openSnackBar("Racun uspesno obrisan", "OK");

          this.racuniTableData.forEach(racun => {
            racun.isSelected = false;
          });

          let tempRacuniTableData = [...this.racuniTableData];

          this.racuniTableData.forEach(rte => {
            if (rte.id == this.racun.id) {
              tempRacuniTableData = tempRacuniTableData.filter(obj => obj !== rte);
            }
          });

          this.racuniTableData = tempRacuniTableData;

          this.selektovaniRacun = undefined;
          this.racun = undefined;
          this.racunIdUneditable = "";
          this.racunSelektovan = false;
        }, errRes => {
          this.openSnackBar("Greska pri brisanju racuna", "OK");
        }
      )
    } else {
      this.openSnackBar("Racun nije selektovan", "OK");
    }
  }
}
