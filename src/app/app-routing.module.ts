import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormOneComponent } from './form-one/form-one.component';
import { ZahtevPickerComponent } from './form-one/zahtev-picker/zahtev-picker.component';
import { FormTwoComponent } from './form-two/form-two.component';
import { RequestPickerComponent } from './form-two/request-picker/request-picker.component';
import { IzmenaRacunaComponent } from './izmena-racuna/izmena-racuna.component';
import { RacunComponent } from './racun/racun.component';


const routes: Routes = [
  {path:'formOne', component: FormOneComponent},
  {path:'formOne/pretragaZahteva', component: ZahtevPickerComponent},
  {path:'formTwo', component: FormTwoComponent},
  {path:'formTwo/pretragaZahteva', component: RequestPickerComponent},
  {path:'racun', component: RacunComponent},
  {path:'izmenaRacuna', component: IzmenaRacunaComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
