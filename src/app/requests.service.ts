import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Zahtev } from './form-one/zahtev.model';
import { Sertifikat } from './form-two/sertifikat.model';
import { DeliveryNote } from './racun/delivery-note.model';
import { PaymentMethod } from './racun/payment-method.modal';
import { Product } from './racun/product.model';
import { ReceiptItem } from './racun/receipt-item.model';
import { ReceiptSaveJson } from './racun/receipt-save-json.model';
import { Receipt } from './racun/receipt.model';
import { Unit } from './racun/unit.model';
import { Worker } from './racun/worker.model';

@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(
    private http: HttpClient
  ) { }

  async getQualityRequestsById(id: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/requests?requestId=${id}`, {headers: header}).toPromise();

      let zahtevList: Zahtev[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Zahtev(
          resZahtev.id,
          resZahtev.productId,
          resZahtev.workerId,
          resZahtev.veterinarianId,
          new Date(resZahtev.date),
          resZahtev.note
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getQualityRequestsById error:", error);

      throw error;
    }
  }

  async getQualityRequests(){
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/requests`, {headers: header}).toPromise();

      let zahtevList: Zahtev[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Zahtev(
          resZahtev.id,
          resZahtev.productId,
          resZahtev.workerId,
          resZahtev.veterinarianId,
          new Date(resZahtev.date),
          resZahtev.note
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getQualityRequests error:", error);

      throw error;
    }
  }

  async getQualityRequest(){
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/requests`, {headers: header}).toPromise();

      let zahtevList: Zahtev[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Zahtev(
          resZahtev.id,
          resZahtev.productId,
          resZahtev.workerId,
          resZahtev.veterinarianId,
          new Date(resZahtev.date),
          resZahtev.note
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getQualityRequestsByDate error:", error);

      throw error;
    }
  }

  async getQualityRequestsByDate(date: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/requests?date=${date}`, {headers: header}).toPromise();

      let zahtevList: Zahtev[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Zahtev(
          resZahtev.id,
          resZahtev.productId,
          resZahtev.workerId,
          resZahtev.veterinarianId,
          new Date(resZahtev.date),
          resZahtev.note
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getQualityRequestsByDate error:", error);

      throw error;
    }
  }

  async getQualityRequestsByIdAndDate(id: string, date: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/requests?requestId=${id}&date=${date}`, {headers: header}).toPromise();

      let zahtevList: Zahtev[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Zahtev(
          resZahtev.id,
          resZahtev.productId,
          resZahtev.workerId,
          resZahtev.veterinarianId,
          new Date(resZahtev.date),
          resZahtev.note
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getQualityRequestsByIdAndDate error:", error);

      throw error;
    }
  }

  async saveQualityRequest(id: number, date: string, description: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    let requestObject = {
      "qualityRequestId": id,
      "date": date,
      "description": description
    }

    try {
      let response: any = await this.http.post(`${environment.server}/milk-factory/quality-certificates`, {...requestObject}, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("saveQualityRequest error:", error);

      throw error;
    }
  }

  async getPotvrdeOKvalitetuById(id: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/quality-certificates?certificateId=${id}`, {headers: header}).toPromise();

      let zahtevList: Sertifikat[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Sertifikat(
          resZahtev.id,
          resZahtev.qualityRequestId,
          resZahtev.date,
          resZahtev.description
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getPotvrdeOKvalitetuById error:", error);

      throw error;
    }
  }

  async getPotvrdeOKvalitetuByDate(date: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/quality-certificates?date=${date}`, {headers: header}).toPromise();

      let zahtevList: Sertifikat[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Sertifikat(
          resZahtev.id,
          resZahtev.qualityRequestId,
          resZahtev.date,
          resZahtev.description
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getPotvrdeOKvalitetuByDate error:", error);

      throw error;
    }
  }

  async getPotvrdeOKvalitetu(){
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/quality-certificates`, {headers: header}).toPromise();

      let zahtevList: Sertifikat[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Sertifikat(
          resZahtev.id,
          resZahtev.qualityRequestId,
          resZahtev.date,
          resZahtev.description
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getPotvrdeOKvalitetuByIdAndDate error:", error);

      throw error;
    }
  }

  async getPotvrdeOKvalitetuByIdAndDate(id: string, date: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/quality-certificates?certificateId=${id}&date=${date}`, {headers: header}).toPromise();

      let zahtevList: Sertifikat[] = [];

      response.data.forEach(resZahtev => {
        const newZahtev = new Sertifikat(
          resZahtev.id,
          resZahtev.qualityRequestId,
          resZahtev.date,
          resZahtev.description
        )

        zahtevList.push(newZahtev);
      });

      return zahtevList;
    } catch (error) {
      console.log("getPotvrdeOKvalitetuByIdAndDate error:", error);

      throw error;
    }
  }

  async editQualityRequest(id: number, certificateId: number, date: string, description: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    let requestObject = {};

    requestObject = {
      "qualityRequestId": id,
      "description": description,
      "date": date
    }

    try {
      let response: any = await this.http.put(`${environment.server}/milk-factory/quality-certificates/${certificateId}`, {...requestObject}, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("editQualityRequest error:", error);

      throw error;
    }
  }

  async deleteQualityRequest(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.delete(`${environment.server}/milk-factory/quality-certificates/${id}`, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("deleteQualityRequest error:", error);

      throw error;
    }
  }

  async getPaymentMethods() {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/payment-methods`, {headers: header}).toPromise();

      let paymentMethods: PaymentMethod[] = [];

      response.data.forEach(paymentMethod => {
        paymentMethods.push(paymentMethod);
      });

      return paymentMethods;
    } catch (error) {
      console.log("getPaymentMethods error:", error);

      throw error;
    }
  }

  async getWorkers() {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.get(`${environment.server}/milk-factory/workers`, {headers: header}).toPromise();

      let workers: Worker[] = [];

      response.data.forEach(worker => {
        const newWorker = new Worker(
          worker.workerId,
          worker.firstLastName
        )

        workers.push(newWorker);
      });

      return workers;
    } catch (error) {
      console.log("getWorkers error:", error);

      throw error;
    }
  }

  async getDeliveryNotes(id: string, date: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    let url: string = `${environment.server}/milk-factory/delivery-notes`;

    if (id != "" || date != "") {
      url += '?';
    }

    if (id != "") {
      url += `noteId=${id}`;

      if (date != "") {
        url += '&'
      }
    }

    if (date != "") {
      url += `date=${date}`;
    }

    try {
      let response: any = await this.http.get(url, {headers: header}).toPromise();

      let deliveryNotes: DeliveryNote[] = [];

      response.data.forEach(deliveryNote => {
        const newDeliveryNote = new DeliveryNote(
          deliveryNote.id,
          deliveryNote.customerId,
          deliveryNote.dispatchDate,
          deliveryNote.workerDelivered,
          deliveryNote.workerIssued
        )

        deliveryNotes.push(newDeliveryNote);
      });

      return deliveryNotes;
    } catch (error) {
      console.log("getDeliveryNotes error:", error);

      throw error;
    }
  }

  async getProducts(id: string, name: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    let url: string = `${environment.server}/milk-factory/products`;

    if (id != "" || name != "") {
      url += '?';
    }

    if (id != "") {
      url += `productId=${id}`;

      if (name != "") {
        url += '&'
      }
    }

    if (name != "") {
      url += `name=${name}`;
    }

    try {
      let response: any = await this.http.get(url, {headers: header}).toPromise();

      let products: Product[] = [];

      response.data.forEach(product => {
        const newProduct = new Product(
          product.id,
          product.name,
          new Unit(product.unit.id, product.unit.name),
          product.dateCreated,
          product.price,
          product.productTypeId,
          product.quantityOnStock
        )

        products.push(newProduct);
      });

      return products;
    } catch (error) {
      console.log("getProducts error:", error);

      throw error;
    }
  }

  async saveReceipt(racun: ReceiptSaveJson) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.post(`${environment.server}/milk-factory/customer-receipts`, {...racun}, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("saveReceipt error:", error);

      throw error;
    }
  }

  async getReceipts(id: string, date: string) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    let url: string = `${environment.server}/milk-factory/customer-receipts`;

    if (id != "" || date != "") {
      url += '?';
    }

    if (id != "") {
      url += `customerReceiptId=${id}`;

      if (date != "") {
        url += '&'
      }
    }

    if (date != "") {
      url += `date=${date}`;
    }

    try {
      let response: any = await this.http.get(url, {headers: header}).toPromise();

      // get prodcuts because receiptItem needs product name
      let productsRes: any = await this.http.get(`${environment.server}/milk-factory/products`, {headers:header}).toPromise();

      const products: Product[] = [];

      productsRes.data.forEach(product => {
        const newProduct = new Product(
          product.id,
          product.name,
          new Unit(product.unit.id, product.unit.name),
          product.dateCreated,
          product.price,
          product.productTypeId,
          product.quantityOnStock
        )

        products.push(newProduct);
      })

      let receipts: Receipt[] = [];

      response.data.forEach(receipt => {
        const paymentMethods: PaymentMethod[] = [];

        receipt.paymentMethods.forEach(paymentMethod => {
          const newPaymentMethod = new PaymentMethod(
            paymentMethod.id,
            paymentMethod.name
          )

          paymentMethods.push(newPaymentMethod);
        });

        const receiptItems: ReceiptItem[] = [];

        let productName: string = "";

        receipt.items.forEach(item => {
          products.forEach(product => {
            if (product.id == item.productId) {
              productName = product.name;
            }
          });

          const newReceiptItem = new ReceiptItem(
            item.id,
            item.productId,
            item.quantity,
            item.rebate,
            item.priceWithDiscount,
            productName
          )

          receiptItems.push(newReceiptItem);
        });

        const newDeliveryNote: DeliveryNote = new DeliveryNote(
          receipt.deliveryNoteDTO.id,
          receipt.deliveryNoteDTO.customerId,
          receipt.deliveryNoteDTO.dispatchDate,
          receipt.deliveryNoteDTO.workerDelivered,
          receipt.deliveryNoteDTO.workerIssued
        )


        const newReceipt = new Receipt(
          receipt.id,
          receipt.dueDate,
          receipt.note,
          paymentMethods,
          receiptItems,
          newDeliveryNote,
          receipt.workerId,
          receipt.customerId
        );

        receipts.push(newReceipt);
      });

      return receipts;
    } catch (error) {
      console.log("getReceipts error:", error);

      throw error;
    }
  }

  async editReceipt(id: number, racun) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.put(`${environment.server}/milk-factory/customer-receipts/${id}`, {...racun}, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("editReceipt error:", error);

      throw error;
    }
  }

  async deleteRacun(id: number) {
    let headerJson = {
      'Content-Type':"application/json",
    }

    let header: HttpHeaders = new HttpHeaders(headerJson);

    try {
      let response: any = await this.http.delete(`${environment.server}/milk-factory/customer-receipts/${id}`, {headers: header}).toPromise();
      return;
    } catch (error) {
      console.log("deleteRacun error:", error);

      throw error;
    }
  }
}

