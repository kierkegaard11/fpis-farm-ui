import { TestBed } from '@angular/core/testing';

import { ZahtevDataTransferHandlerService } from './zahtev-data-transfer-handler.service';

describe('ZahtevDataTransferHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ZahtevDataTransferHandlerService = TestBed.get(ZahtevDataTransferHandlerService);
    expect(service).toBeTruthy();
  });
});
