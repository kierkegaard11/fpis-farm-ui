import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZahtevPickerComponent } from './zahtev-picker.component';

describe('ZahtevPickerComponent', () => {
  let component: ZahtevPickerComponent;
  let fixture: ComponentFixture<ZahtevPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZahtevPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZahtevPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
