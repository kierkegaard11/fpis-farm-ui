import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { RequestsService } from '../requests.service';
import { ZahtevDataTransferHandlerService } from './zahtev-data-transfer-handler.service';
import { Zahtev } from './zahtev.model';

@Component({
  selector: 'app-form-one',
  templateUrl: './form-one.component.html',
  styleUrls: ['./form-one.component.scss']
})
export class FormOneComponent implements OnInit {

  date: string = "";
  dateErrorMessage: string = "";

  description: string = "";
  descriptionErrorMessage: string = "";

  tableData: Zahtev[] = [];

  displayColumns: string[] = ['zahtevId', 'proizvodId', 'radnikId', 'veterinarId', 'datum', 'napomena'];

  constructor(
    private datepipe: DatePipe,
    private zDTHS: ZahtevDataTransferHandlerService,
    private snackBar: MatSnackBar,
    private requestsService: RequestsService
  ) { }

  ngOnInit() {
    if (this.zDTHS.readyToReceive) {
      this.tableData.push(this.zDTHS.zahtevToTransfer);
      this.zDTHS.readyToReceive = false;
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  onOdustani() {
    this.tableData = [];
    this.date = "";
    this.description = "";
    this.dateErrorMessage = "";
    this.descriptionErrorMessage = "";
  }

  onSacuvaj() {
    this.dateErrorMessage = "";
    this.descriptionErrorMessage = "";

    if (this.tableData.length == 0) {
      this.openSnackBar('Zahtev nije izabran. Prvo odaberite zahtev klikom na dugme "Pretraga zahteva".', "OK");
      return;
    }

    if (this.validateDatum()) {
      this.requestsService.saveQualityRequest(this.tableData[0].zahtevId, this.date, this.description).then(
        (responseData: any) => {
          this.openSnackBar("Potvrda uspesno sacuvana.", "OK");

          this.date = "";
          this.description = "";
          this.tableData = [];
        }, errRes => {
          this.openSnackBar("Greska pri cuvanju potvrde.", "OK");
        }
      );
    }
  }

  validateDatum() {
    if (this.date.length == 0) {
      this.dateErrorMessage = "Datum zahteva je obavezno polje.";
      return false;
    }

    if (!this.isValidDateFormat(this.date)) {
      this.dateErrorMessage = "Datum zahteva nije u formatu yyyy-mm-dd.";
      return false;
    }

    if (Number(this.date.split("-")[1]) == 0 || Number(this.date.split("-")[1]) > 12) {
      this.dateErrorMessage = "Datum zahteva nije ispravan.";
      return false;
    }

    if (Number(this.date.split("-")[2]) == 0 || Number(this.date.split("-")[2]) > 31) {
      this.dateErrorMessage = "Datum zahteva nije ispravan.";
      return false;
    }

    this.dateErrorMessage = "";
    return true;
  }

  isValidDateFormat(str: string) {
    return /^\d{4}\-\d{1,2}\-\d{1,2}$/.test(str);
  }
}
