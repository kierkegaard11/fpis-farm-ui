import { Injectable } from '@angular/core';
import { Zahtev } from './zahtev.model';

@Injectable({
  providedIn: 'root'
})
export class ZahtevDataTransferHandlerService {
  zahtevToTransfer: Zahtev;
  readyToReceive: boolean = false;

  constructor() { }
}
