export class ZahtevTableEntry {
  constructor(
      public zahtevId: number,
      public proizvodId: number,
      public radnikId: number,
      public veterinarId: number,
      public datum: Date,
      public napomena: string,
      public isSelected: boolean
  ) {}
}
