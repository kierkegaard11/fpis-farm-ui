export class SertifikatTableEntry {
  constructor(
      public potvrdaId: number,
      public zahtevId: number,
      public datumPotvrde: Date,
      public opisPotvrde: string,
      public isSelected: boolean
  ) {}
}
