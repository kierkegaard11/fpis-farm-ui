import { Injectable } from '@angular/core';
import { Zahtev } from '../form-one/zahtev.model';

@Injectable({
  providedIn: 'root'
})
export class RequestDataTransferHandlerService {
  zahtevToTransfer: Zahtev;
  readyToReceive: boolean = false;

  constructor() { }
}
