import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar, throwToolbarMixedModesError } from '@angular/material';
import { Zahtev } from '../form-one/zahtev.model';
import { ZahtevTableEntry } from '../form-one/zahtevTableEntry.modal';
import { RequestsService } from '../requests.service';
import { RequestDataTransferHandlerService } from './request-data-transfer-handler.service';
import { Sertifikat } from './sertifikat.model';
import { SertifikatTableEntry } from './sertifikatTableEntry.model';

@Component({
  selector: 'app-form-two',
  templateUrl: './form-two.component.html',
  styleUrls: ['./form-two.component.scss']
})
export class FormTwoComponent implements OnInit {

  potvrdaOKvalitetuId: string = "";
  potvrdaOKvalitetuErrorMessage: string = "";

  potvrdaOKvalitetuIdUneditable: string = "";

  datumKreiranjaPotvrde: string = "";
  datumKreiranjaPotvrdeErrorMessage: string = "";

  date: string = "";
  dateErrorMessage: string = "";

  description: string = "";
  descriptionErrorMessage: string = "";

  selectedSertifikatTableEntry: SertifikatTableEntry;

  tableData: SertifikatTableEntry[] = [];

  displayColumns: string[] = ['potvrdaId', 'zahtevId', 'datumPotvrde', 'opisPotvrde'];

  tableDataTwo: ZahtevTableEntry[] = [];

  displayColumnsTwo: string[] = ['zahtevId', 'proizvodId', 'radnikId', 'veterinarId', 'datum', 'napomena'];

  constructor(
    private datepipe: DatePipe,
    private snackBar: MatSnackBar,
    private requestsService: RequestsService,
    private rDTHS: RequestDataTransferHandlerService
  ) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('formTwoTableOne')) != null || JSON.parse(localStorage.getItem('formTwoTableOne')) != undefined) {
      this.tableData = JSON.parse(localStorage.getItem('formTwoTableOne'));
    }

    if (JSON.parse(localStorage.getItem('selectedSertifikat')) != null || JSON.parse(localStorage.getItem('selectedSertifikat')) != undefined) {
      this.selectedSertifikatTableEntry = JSON.parse(localStorage.getItem('selectedSertifikat'));
      this.potvrdaOKvalitetuIdUneditable = this.selectedSertifikatTableEntry.potvrdaId.toString();

      this.date = this.dateFormat(this.selectedSertifikatTableEntry.datumPotvrde);
      this.description = this.selectedSertifikatTableEntry.opisPotvrde;
    }

    if (JSON.parse(localStorage.getItem('formTwoTableTwo')) != null || JSON.parse(localStorage.getItem('formTwoTableTwo')) != undefined) {
      this.tableDataTwo = [];
      this.tableDataTwo.push(JSON.parse(localStorage.getItem('formTwoTableTwo')));
    }

    if (this.rDTHS.readyToReceive) {
      this.tableDataTwo = [];
      if (this.rDTHS.readyToReceive) {
        this.tableDataTwo.push(this.convertZahtevToZahtevTableEntry(this.rDTHS.zahtevToTransfer));
        this.rDTHS.readyToReceive = false;
      }
    }
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  onPretragaPotvrde() {
    this.tableData = [];

    this.potvrdaOKvalitetuErrorMessage = "";
    this.datumKreiranjaPotvrdeErrorMessage = "";

    if (this.potvrdaOKvalitetuId == "" && this.datumKreiranjaPotvrde == "") {
      this.requestPotvrdaOKvalitetu();
    }

    if (this.potvrdaOKvalitetuId != "" && this.datumKreiranjaPotvrde == "") {
      if (this.validatePotvrdaOKvalitetuId()) {
        this.requestPotvrdaOKvalitetuById();
      }
    }

    if (this.potvrdaOKvalitetuId == "" && this.datumKreiranjaPotvrde != "") {
      if (this.validateDatumKreiranjaPotvrde()) {
        this.requestPotvrdaOKvalitetuByDate();
      }
    }

    if (this.potvrdaOKvalitetuId != "" && this.datumKreiranjaPotvrde != "") {
      if (this.validatePotvrdaOKvalitetuId() && this.validateDatumKreiranjaPotvrde()) {
        this.requestPotvrdaOKvalitetuByIdAndDate();
      }
    }
  }

  requestPotvrdaOKvalitetuById() {
    this.requestsService.getPotvrdeOKvalitetuById(this.potvrdaOKvalitetuId).then(
      (sertifikatList: any) => {
        console.log("zahtevByIdResponseAfterHandling", sertifikatList);

        let newTableData: SertifikatTableEntry[] = [];

        sertifikatList.forEach(sertifikat => {
          newTableData.push(this.convertSertifikatToSertifikatTableEntry(sertifikat));
        });

        this.tableData = newTableData;
        localStorage.setItem('formTwoTableOne', JSON.stringify(newTableData));
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju potvrde po id-u", "OK");
      }
    );
  }

  requestPotvrdaOKvalitetu(){
    this.requestsService.getPotvrdeOKvalitetu().then(
      (sertifikatList: any) => {
        console.log("zahtevResponseAfterHandling", sertifikatList);

        let newTableData: SertifikatTableEntry[] = [];

        sertifikatList.forEach(sertifikat => {
          newTableData.push(this.convertSertifikatToSertifikatTableEntry(sertifikat));
        });

        this.tableData = newTableData;
        localStorage.setItem('formTwoTableOne', JSON.stringify(newTableData));
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju potvrda", "OK");
      }
    );
  }

  requestPotvrdaOKvalitetuByDate() {
    this.requestsService.getPotvrdeOKvalitetuByDate(this.datumKreiranjaPotvrde).then(
      (sertifikatList: any) => {
        console.log("zahtevByDateResponseAfterHandling", sertifikatList);

        let newTableData: SertifikatTableEntry[] = [];

        sertifikatList.forEach(sertifikat => {
          newTableData.push(this.convertSertifikatToSertifikatTableEntry(sertifikat));
        });

        this.tableData = newTableData;
        localStorage.setItem('formTwoTableOne', JSON.stringify(newTableData));
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju potvrde po datumu", "OK");
      }
    );
  }

  requestPotvrdaOKvalitetuByIdAndDate() {
    this.requestsService.getPotvrdeOKvalitetuByIdAndDate(this.potvrdaOKvalitetuId, this.datumKreiranjaPotvrde).then(
      (sertifikatList: any) => {
        console.log("zahtevByIdAndDateResponseAfterHandling", sertifikatList);

        let newTableData: SertifikatTableEntry[] = [];

        sertifikatList.forEach(sertifikat => {
          newTableData.push(this.convertSertifikatToSertifikatTableEntry(sertifikat));
        });

        this.tableData = newTableData;
        localStorage.setItem('formTwoTableOne', JSON.stringify(newTableData));
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju potvrde po id-u i datumu", "OK");
      }
    );
  }

  validatePotvrdaOKvalitetuId() {
    if (!Number.isInteger(Number(this.potvrdaOKvalitetuId))) {
      this.potvrdaOKvalitetuErrorMessage = "Sifra zahteva mora biti celobrojna vrednost.";
      return false;
    }

    if (Number(this.potvrdaOKvalitetuId) == 0) {
      this.potvrdaOKvalitetuErrorMessage = "Sifra zahteva ne moze biti 0.";
      return false;
    }

    if (this.potvrdaOKvalitetuId.charAt(0) == '0') {
      this.potvrdaOKvalitetuErrorMessage = "Sifra zahteva ne moze poceti sa 0.";
      return false;
    }

    this.potvrdaOKvalitetuErrorMessage = "";
    return true;
  }

  validateDatumKreiranjaPotvrde() {
    if (this.datumKreiranjaPotvrde.length == 0) {
      this.datumKreiranjaPotvrdeErrorMessage = "Datum zahteva je obavezno polje.";
      return false;
    }

    if (!this.isValidDateFormat(this.datumKreiranjaPotvrde)) {
      this.datumKreiranjaPotvrdeErrorMessage = "Datum zahteva nije u formatu yyyy-mm-dd.";
      return false;
    }

    if (Number(this.datumKreiranjaPotvrde.split("-")[1]) == 0 || Number(this.datumKreiranjaPotvrde.split("-")[1]) > 12) {
      this.datumKreiranjaPotvrdeErrorMessage = "Datum zahteva nije ispravan.";
      return false;
    }

    if (Number(this.datumKreiranjaPotvrde.split("-")[2]) == 0 || Number(this.datumKreiranjaPotvrde.split("-")[2]) > 31) {
      this.datumKreiranjaPotvrdeErrorMessage = "Datum zahteva nije ispravan.";
      return false;
    }

    this.datumKreiranjaPotvrdeErrorMessage = "";
    return true;
  }

  onIzaberiPotvrdu() {
    if (this.selectedSertifikatTableEntry != null || this.selectedSertifikatTableEntry != undefined) {
      this.potvrdaOKvalitetuIdUneditable = this.selectedSertifikatTableEntry.potvrdaId.toString();
      this.openSnackBar('Potvrda izabrana.', "OK");
      localStorage.setItem('selectedSertifikat', JSON.stringify(this.selectedSertifikatTableEntry));
      console.log("Izabrana potvrda", this.convertSertifikatTableEntryToSertifikat(this.selectedSertifikatTableEntry));
      this.posaljiZahtevZaZahtev(this.selectedSertifikatTableEntry.zahtevId.toString());
      this.date = this.dateFormat(this.selectedSertifikatTableEntry.datumPotvrde);
      this.description = this.selectedSertifikatTableEntry.opisPotvrde;
    } else {
      this.openSnackBar('Potvrda nije izabrana. Prvo odaberite potvrdu klikom na dugme "Pretraga potvrde" i selektovanjem potvrde iz tabele.', "OK");
    }
  }

  posaljiZahtevZaZahtev(id: string) {
    this.requestsService.getQualityRequestsById(id).then(
      (zahtevi: Zahtev[]) => {
        if (zahtevi.length == 1) {
          this.tableDataTwo = [];
          this.tableDataTwo.push(this.convertZahtevToZahtevTableEntry(zahtevi[0]));
          localStorage.setItem('formTwoTableTwo', JSON.stringify(zahtevi[0]));
        } else {
          console.log("Greska pri pribavljanju zahteva", zahtevi);
          this.openSnackBar("Greska pri pribavljanju zahteva", "OK");
        }
      }, errRes => {
        console.log("Greska pri pribavljanju zahteva", errRes);
        this.openSnackBar("Greska pri pribavljanju zahteva", "OK");
      }
    )
  }

  isValidDateFormat(str: string) {
    return /^\d{4}\-\d{1,2}\-\d{1,2}$/.test(str);
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  onRowSelect(object: SertifikatTableEntry) {
    this.tableData.forEach((sertifikat: SertifikatTableEntry) => {
      sertifikat.isSelected = false;
    })

    object.isSelected = true;
    this.selectedSertifikatTableEntry = object;

    localStorage.setItem('formTwoTableOne', JSON.stringify(this.tableData));
  }

  convertSertifikatToSertifikatTableEntry(sertifikat: Sertifikat) {
    const ste: SertifikatTableEntry = new SertifikatTableEntry(
      sertifikat.potvrdaId,
      sertifikat.zahtevId,
      sertifikat.datumPotvrde,
      sertifikat.opisPotvrde,
      false
    )

    return ste;
  }

  convertSertifikatTableEntryToSertifikat(ste: Sertifikat) {
    const sertifikat: Sertifikat = new Sertifikat(
      ste.potvrdaId,
      ste.zahtevId,
      ste.datumPotvrde,
      ste.opisPotvrde
    )

    return sertifikat;
  }

  convertZahtevTableEntryToZahtev(zte: ZahtevTableEntry): Zahtev {
    const zahtev: Zahtev = new Zahtev(
      zte.zahtevId,
      zte.proizvodId,
      zte.radnikId,
      zte.veterinarId,
      zte.datum,
      zte.napomena
    )

    return zahtev;
  }

  convertZahtevToZahtevTableEntry(zahtev: Zahtev) {
    const zte: ZahtevTableEntry = new ZahtevTableEntry(
      zahtev.zahtevId,
      zahtev.proizvodId,
      zahtev.radnikId,
      zahtev.veterinarId,
      zahtev.datum,
      zahtev.napomena,
      false
    )

    return zte;
  }

  onOdustani() {
    this.tableData = [];
    this.tableDataTwo = [];
    this.potvrdaOKvalitetuIdUneditable = "";
    this.date = "";
    this.dateErrorMessage = "";
    this.description = "";
    this.descriptionErrorMessage = "";
    this.potvrdaOKvalitetuId = "";
    this.potvrdaOKvalitetuErrorMessage = "";
    this.datumKreiranjaPotvrde = "";
    this.datumKreiranjaPotvrdeErrorMessage = "";
    this.selectedSertifikatTableEntry = null;

    localStorage.setItem('formTwoTableOne', null);
    localStorage.setItem('selectedSertifikat', null);
    localStorage.setItem('formTwoTableOne', null);
  }

  onIzmeniZahtev() {
    this.dateErrorMessage = "";
    this.descriptionErrorMessage = "";

    if (this.tableDataTwo.length == 0) {
      this.openSnackBar('Zahtev nije izabran. Prvo odaberite zahtev klikom na dugme "Pretraga zahteva".', "OK");
      return;
    }

    if (!this.validateDatum()) {
      return;
    }

    this.requestsService.editQualityRequest(this.tableDataTwo[0].zahtevId, this.selectedSertifikatTableEntry.potvrdaId, this.date, this.description).then(
      () => {
        this.openSnackBar("Potvrda uspesno izmenjena.", "OK");

        this.onOdustani();
      }, errRes => {
        this.openSnackBar("Greska pri izmeni potvrde.", "OK");
      }
    );
  }

  validateDatum() {
    if (this.date.length == 0) {
      this.dateErrorMessage = "Datum je obavezno polje.";
      return false;
    }

    if (!this.isValidDateFormat(this.date)) {
      this.dateErrorMessage = "Datum nije u formatu yyyy-mm-dd.";
      return false;
    }

    if (Number(this.date.split("-")[1]) == 0 || Number(this.date.split("-")[1]) > 12) {
      this.dateErrorMessage = "Datum nije ispravan.";
      return false;
    }

    if (Number(this.date.split("-")[2]) == 0 || Number(this.date.split("-")[2]) > 31) {
      this.dateErrorMessage = "Datum nije ispravan.";
      return false;
    }

    this.dateErrorMessage = "";
    return true;
  }

  onObrisiZahtev() {
    this.dateErrorMessage = "";
    this.descriptionErrorMessage = "";

    try {
      this.requestsService.deleteQualityRequest(+this.potvrdaOKvalitetuIdUneditable).then(
        () => {
          this.openSnackBar("Potvrda uspesno obrisana.", "OK");

          this.date = "";
          this.description = "";
          this.tableDataTwo = [];

          this.onOdustani();
        }, errRes => {
          this.openSnackBar("Greska pri brisanju potvrde.", "OK");
        }
      );
    } catch (err) {
      this.openSnackBar("Greska pri brisanju potvrde.", "OK");
    }

  }
}
