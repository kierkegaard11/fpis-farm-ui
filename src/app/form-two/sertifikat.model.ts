export class Sertifikat {
  constructor(
      public potvrdaId: number,
      public zahtevId: number,
      public datumPotvrde: Date,
      public opisPotvrde: string,
  ) {}
}
