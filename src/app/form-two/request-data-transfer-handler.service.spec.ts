import { TestBed } from '@angular/core/testing';

import { RequestDataTransferHandlerService } from './request-data-transfer-handler.service';

describe('RequestDataTransferHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestDataTransferHandlerService = TestBed.get(RequestDataTransferHandlerService);
    expect(service).toBeTruthy();
  });
});
