import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Zahtev } from 'src/app/form-one/zahtev.model';
import { ZahtevTableEntry } from 'src/app/form-one/zahtevTableEntry.modal';
import { RequestsService } from 'src/app/requests.service';
import { RequestDataTransferHandlerService } from '../request-data-transfer-handler.service';

@Component({
  selector: 'app-request-picker',
  templateUrl: './request-picker.component.html',
  styleUrls: ['./request-picker.component.scss']
})
export class RequestPickerComponent implements OnInit {
  sifraZahteva: string = "";
  sifraZahtevaErrorMessage: string = "";

  datumZahteva: string = "";
  datumZahtevaErrorMessage: string = "";

  tableData: ZahtevTableEntry[] = [];

  displayColumns: string[] = ['zahtevId', 'proizvodId', 'radnikId', 'veterinarId', 'datum', 'napomena'];

  selectedZahtevTableEntry: ZahtevTableEntry;

  constructor(
    private datepipe: DatePipe,
    private rDTHS: RequestDataTransferHandlerService,
    private router: Router,
    private snackBar: MatSnackBar,
    private requestsService: RequestsService
  ) { }

  ngOnInit() {
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action);
  }

  dateFormat(date){
    return this.datepipe.transform(date, 'yyyy-MM-dd');
  }

  onPretragaZahteva() {
    this.tableData = [];

    this.sifraZahtevaErrorMessage = "";
    this.datumZahtevaErrorMessage = "";

    if (this.sifraZahteva == "" && this.datumZahteva == "") {
      this.requestZahtev();
    }

    if (this.sifraZahteva != "" && this.datumZahteva == "") {
      if (this.validateSifra()) {
        this.requestZahtevDataById();
      }
    }

    if (this.sifraZahteva == "" && this.datumZahteva != "") {
      if (this.validateDatum()) {
        this.requestZahtevDataByDate();
      }
    }

    if (this.sifraZahteva != "" && this.datumZahteva != "") {
      if (this.validateSifra() && this.validateDatum()) {
        this.requestZahtevDataByIdAndDate();
      }
    }
  }

  requestZahtevDataById() {
    this.requestsService.getQualityRequestsById(this.sifraZahteva).then(
      (zahtevList: any) => {
        console.log("zahtevByIdResponseAfterHandling", zahtevList);

        const newTableData: ZahtevTableEntry[] = [];

        zahtevList.forEach(zahtev => {
          newTableData.push(this.convertZahtevToZahtevTableEntry(zahtev));
        });

        this.tableData = newTableData;
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju zahteva po id-u", "OK");
      }
    );
  }

  requestZahtev(){
    this.requestsService.getQualityRequests().then(
      (zahtevList: any) => {
        console.log("zahteviResponseAfterHandling", zahtevList);

        const newTableData: ZahtevTableEntry[] = [];

        zahtevList.forEach(zahtev => {
          newTableData.push(this.convertZahtevToZahtevTableEntry(zahtev));
        });

        this.tableData = newTableData;
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju zahteva", "OK");
      }
    );
  }

  requestZahtevDataByDate() {
    this.requestsService.getQualityRequestsByDate(this.datumZahteva).then(
      (zahtevList: any) => {
        console.log("zahteviByDateResponseAfterHandling", zahtevList);

        const newTableData: ZahtevTableEntry[] = [];

        zahtevList.forEach(zahtev => {
          newTableData.push(this.convertZahtevToZahtevTableEntry(zahtev));
        });

        this.tableData = newTableData;
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju zahteva po datumu", "OK");
      }
    );
  }

  requestZahtevDataByIdAndDate() {
    this.requestsService.getQualityRequestsByIdAndDate(this.sifraZahteva, this.datumZahteva).then(
      (zahtevList: any) => {
        console.log("zahteviByIdAndDateResponseAfterHandling", zahtevList);

        const newTableData: ZahtevTableEntry[] = [];

        zahtevList.forEach(zahtev => {
          newTableData.push(this.convertZahtevToZahtevTableEntry(zahtev));
        });

        this.tableData = newTableData;
      }, errRes => {
        this.openSnackBar("Greska pri pribavaljanju zahteva po id-u i datumu", "OK");
      }
    );
  }

  validateSifra() {
    if (!Number.isInteger(Number(this.sifraZahteva))) {
      this.sifraZahtevaErrorMessage = "Sifra zahteva mora biti celobrojna vrednost.";
      return false;
    }

    if (Number(this.sifraZahteva) == 0) {
      this.sifraZahtevaErrorMessage = "Sifra zahteva ne moze biti 0.";
      return false;
    }

    if (this.sifraZahteva.charAt(0) == '0') {
      this.sifraZahtevaErrorMessage = "Sifra zahteva ne moze poceti sa 0.";
      return false;
    }

    this.sifraZahtevaErrorMessage = "";
    return true;
  }

  validateDatum() {
    if (!this.isValidDateFormat(this.datumZahteva)) {
      this.datumZahtevaErrorMessage = "Datum zahteva nije u formatu yyyy-mm-dd.";
      return false;
    }

    if (Number(this.datumZahteva.split("-")[1]) == 0 || Number(this.datumZahteva.split("-")[1]) > 12) {
      this.datumZahtevaErrorMessage = "Datum zahteva nije ispravan.";
      return false;
    }

    if (Number(this.datumZahteva.split("-")[2]) == 0 || Number(this.datumZahteva.split("-")[2]) > 31) {
      this.datumZahtevaErrorMessage = "Datum zahteva nije ispravan.";
      return false;
    }

    this.datumZahtevaErrorMessage = "";
    return true;
  }

  isValidDateFormat(str: string) {
    return /^\d{4}\-\d{1,2}\-\d{1,2}$/.test(str);
  }

  onRowSelect(object: ZahtevTableEntry) {
    this.tableData.forEach((zte: ZahtevTableEntry) => {
      zte.isSelected = false;
    })

    object.isSelected = true;
    this.selectedZahtevTableEntry = object;
  }

  convertZahtevTableEntryToZahtev(zte: ZahtevTableEntry): Zahtev {
    const zahtev: Zahtev = new Zahtev(
      zte.zahtevId,
      zte.proizvodId,
      zte.radnikId,
      zte.veterinarId,
      zte.datum,
      zte.napomena
    )

    return zahtev;
  }

  convertZahtevToZahtevTableEntry(zahtev: Zahtev) {
    const zte: ZahtevTableEntry = new ZahtevTableEntry(
      zahtev.zahtevId,
      zahtev.proizvodId,
      zahtev.radnikId,
      zahtev.veterinarId,
      zahtev.datum,
      zahtev.napomena,
      false
    )

    return zte;
  }

  onIzaberiZahtev() {
    if (this.selectedZahtevTableEntry != null || this.selectedZahtevTableEntry != undefined) {
      this.rDTHS.zahtevToTransfer = this.convertZahtevTableEntryToZahtev(this.selectedZahtevTableEntry);
      this.rDTHS.readyToReceive = true;

      console.log("PRETRAGA ZAHTEVA DATA SENT BACK", this.rDTHS.zahtevToTransfer, this.rDTHS.readyToReceive);

      this.router.navigate(['formTwo']);
    } else {
      this.openSnackBar('Zahtev nije izabran. Odaberite zahtev iz tabele klikom na dugme "Pretraga zahteva"', "OK");
    }
  }

}
