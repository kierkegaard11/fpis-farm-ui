import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestPickerComponent } from './request-picker.component';

describe('RequestPickerComponent', () => {
  let component: RequestPickerComponent;
  let fixture: ComponentFixture<RequestPickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestPickerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
