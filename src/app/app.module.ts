import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatDatepickerModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatMenuModule, MatNativeDateModule, MatProgressSpinnerModule, MatSelectModule, MatSnackBarModule, MatSpinner } from '@angular/material';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FormOneComponent } from './form-one/form-one.component';

import { MatTableModule } from '@angular/material/table'
import { DatePipe } from '@angular/common';
import { ZahtevPickerComponent } from './form-one/zahtev-picker/zahtev-picker.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormTwoComponent } from './form-two/form-two.component';
import { RequestPickerComponent } from './form-two/request-picker/request-picker.component';
import { RacunComponent } from './racun/racun.component';
import { PretragaOtpremniceModalComponent } from './racun/pretraga-otpremnice-modal/pretraga-otpremnice-modal.component';
import { PretragaProizvodaModalComponent } from './racun/pretraga-proizvoda-modal/pretraga-proizvoda-modal.component';
import { IzmenaRacunaComponent } from './izmena-racuna/izmena-racuna.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    FormOneComponent,
    ZahtevPickerComponent,
    FormTwoComponent,
    RequestPickerComponent,
    RacunComponent,
    PretragaOtpremniceModalComponent,
    PretragaProizvodaModalComponent,
    IzmenaRacunaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    HttpClientModule,
    MatMenuModule,
    MatSelectModule,
    MatDialogModule,
    MatProgressSpinnerModule
  ],
  exports: [
    MatTableModule,
    MatFormFieldModule,
    MatInputModule
  ],
  entryComponents: [PretragaOtpremniceModalComponent, PretragaProizvodaModalComponent],
  providers: [DatePipe],
  bootstrap: [AppComponent],
})
export class AppModule { }
